#pip install pymongo  // pip install pymongo[srv]
#pip install bs4
from pymongo import MongoClient
from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import pprint

client = MongoClient("mongodb+srv://teste:teste@projeto1-niusl.mongodb.net/test?retryWrites=true&w=majority")
db = client['database']

disciplinas = db.disciplinas

def saveDataBase(disc, conte):
  for d, c in zip(disc, conte):
    disciplinas.insert_one({
        "Disciplina": d,
        "Conteudo": c
    }) 

def getSiteLinks(url):
  link = "https://www.dac.unicamp.br/sistemas/catalogos/grad/catalogo2020" + url
  html = urlopen(link)
  bs = BeautifulSoup(html, 'html.parser')
  try:
      disciplina = []
      conteudo = []
      discip_tag = bs.find_all('div', attrs={'class':"ancora"})
      conteudo_tag = bs.find_all('div',attrs={'class':'justificado'})
      for a_tag in discip_tag:
        disciplina.append(a_tag.text)

      for p_tag in conteudo_tag:
        conteudo.append(p_tag.text.replace('Ementa:', ''))

      disciplina.remove('\nDisciplinas\n') 
       
      saveDataBase(disciplina, conteudo)
  except AttributeError:
    print("Página mal formatada.")
d

#TODOS OS LINKS 
html = urlopen('https://www.dac.unicamp.br/sistemas/catalogos/grad/catalogo2020/disciplinas/tipoSI.html')
bs = BeautifulSoup(html, 'html.parser')

for tag in bs.find_all('a', href = re.compile('^(../coordenadorias/)((?!:).)*$')):
  if 'href' in tag.attrs:
    new_url = tag.attrs['href'].replace('..', '')
    getSiteLinks(new_url)

# TESTE COM UM LINK
# getSiteLinks('/coordenadorias/0065/0065.html#SI420')